<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<head>
	<meta charset="utf-8">
	<title>KYB</title>

	<link rel="stylesheet" type="text/css" href="<?= base_url()?>tema/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url()?>tema/css/custom.css">
	 <link rel="stylesheet" type="text/css" href="<?= base_url()?>tema/css/slick/slick-theme.css"/>
	 <link rel="stylesheet" type="text/css" href="<?= base_url()?>tema/css/slick/slick.css"/>
</head>

<header>
		<div class="slider" >

			<div class="slider-header">
			            <div id="img-slider1" ></div>
			            <div id="img-slider2" ></div>
			            <div id="img-slider3" ></div>
             </div>

             <div id="logo">
				<div class="col-sm-12 ">
					<img class="logo img-responsive" src="<?= base_url()?>tema/imgs/logo.png">
					<a href="" class="btn-micuenta">Mi cuenta &nbsp;<font color="#E51616" >i</font></a>
				</div>
 
				
						   
			 </div>

			 


				<div class="container contenido-header">
				
						
						    	 <!--Menu-->
						  			
						  			<center>
						  				<ul class="nav justify-content-center menu">
										    <li class="nav-item">
										      <a class="nav-link" href="#">INICIO</a>
										    </li>
										    <li class="nav-item">
										      <a class="nav-link" href="#">NOSOTROS</a>
										    </li>
										    <li class="nav-item">
										      <a class="nav-link" href="#">AMORTIGUADORES</a>
										    </li>
										    <li class="nav-item">
										      <a class="nav-link" href="#">FACTURAS</a>
										    </li>
										   
										    <li class="nav-item">
										      <a class="nav-link" href="#">CONTACTO</a>
										    </li>
						  				</ul>
						  		    </center>


					             <!--Fin del Menu-->

					             <!--Redes sociales-->
										
										

					             <!--Fin de redes sociales-->

							 		
							 			<div class="texto-header">
										    <br><center><h3 class="subtitle-1">Registrá tu factura y así activás la</h3></center>
										    <h1 class="title-1">Garantía de un año</h1>
								         </div>
								   


								    <div id="registra-factura">
								    	<center><a href=""><img class="img-responsive btn-rojos" src="<?= base_url()?>tema/imgs/svg/btn-registra-factura.svg"></a></center>
									</div>
									


									<!--Flechas -->


									<div class="flecha-row">
										<img class="flechas img-responsive" src="<?= base_url()?>tema/imgs/svg/flecha-left.svg">
										<img class="flechas img-responsive" src="<?= base_url()?>tema/imgs/svg/flecha-right.svg">
									</div> 

						      
						    </div>

						    <div class="red-social-bloque">
											
								<a href=""><img class="redsocial img-responsive" src="<?= base_url()?>tema/imgs/facebook.jpg"></a>
								<a href=""><img class="redsocial img-responsive" src="<?= base_url()?>tema/imgs/youtube.png"></a>
								<a href=""><img class="redsocial img-responsive" src="<?= base_url()?>tema/imgs/whatsapp.png"></a>
							</div>


			   </div>
			 


	         

		
	</header>
<body>

	<!--Nosotros-->

	<div class="container-fluid nosotros" id="nosotros">

		<div class="container">
			<div class="row">
				<h1 class="nosotros-title">nosotros</h1>
			</div>

			<div class="row parrafo-nosotros">
				<p class="parrafos">Especializados en sistemas hidráulicos, con una tecnología ampliamente utilizada en las industrias aeronáutica, ferrovial y marítima. KYB es la empresa líder en sistemas hidráulicos de Japón.</p>
			</div>
			<br>
			<div class="row">
				<p class="parrafos">Su principal tarea es la de absorber todas las irregularidades del camino. Con ellos su vehículo es más estable en cualquier maniobra, en cualquier superficie y en todas las condiciones meteorológicas.Se usa cerca de las ruedas propiamente en la suspensión de los vehículos. Son tubulares en acero, con pistones cromados, están cargados de aceite y algunos traen gas nitrógeno.</p>
			</div>

			<div class="row">
				<div class="col-md-3 margin-40">
					<a href=""> <div class="recuadro-nosotros catalogo ">

					</div></a>
					
				</div>

				<div class="col-md-3 margin-40">
					<a href=""><div class="recuadro-nosotros info-tecnica">

					</div></a>
					
					
				</div>

				<div class="col-md-3 margin-40">
					<a href=""><div class="recuadro-nosotros punto-venta ">

					</div></a>
					
					
				</div>
			</div>
		</div>
		        	
     
    </div>


  <hr style="border:3px solid #fff ;margin:0px">


    <!--Catalogo Banner-->
 	

 	<div class="container-fluid amortiguadores" id="catalogo-banner">

 		<div class="container amortiguador">
 			
 				<div class="col-sm-12">
 					<center><p class="subtitle-amortiguador">marca de amortiguadores</p></center>
 					<center><p class="subtitle-3">de altísima calidad</p></center>
 					<center><img class="img-responsive btn-amort" src="<?= base_url()?>tema/imgs/svg/amortiguadornuevo.svg"></center>
 				</div>

 				<div class="col-sm-12 dispplayinline">
 					<div class="col-sm-6 borde-rojo">

 						<div class="row recuadros-tercera-seccion">
 							<figure><img class="img-responsive imgs-iconos-terceraseccion" src="<?= base_url()?>tema/imgs/svg/tecjaponesa.svg"></figure>
 							<p class="font-blanca-helvetica">TECNOLOGÍA JAPONESA DE MEJORA CONTÍNUA</p>
 						</div>

 						<div class="row">
 							
 						</div>
 						
 					</div>

 					<div class="col-sm-6 borde-rojo">
 						
 						<div class="row recuadros-tercera-seccion">
 							<figure><img class="img-responsive imgs-iconos-terceraseccion" src="<?= base_url()?>tema/imgs/svg/tecjponesa2.svg"></figure>
 							<p class="font-blanca-helvetica">EQUIPO ORIGINAL DE MUCHOS GRANDES FABRICANTES DE VEHÍCULOS</p>
 						</div>

 						<div class="row">
 							
 						</div>
 					</div>
 				</div>

 				
 			
 		</div>
 		
 	</div>

 	 <hr style="border:3px solid #fff ;margin:0px">


    <!--Amortiguadores -->
 	

 	

 		<div class="container-fluid amortiguadores2" id="amortiguadores2">

		<div class="container">
			<div class="row">
				<h1 class="amortiguadores2-title">AMORTIGUADORES</h1>
			</div>

			<br>

			
			
			<div class="row">
				<div class="col-sm-3 ">
					 <div class="borde-rojo">
						<img class="  img-responsive img-amortiguador-catalogo" src="<?= base_url()?>tema/imgs/amort1.png">

						<center><p class="HelveticaNeueLTStd-Lt">Amortiguadores <br>Premium </p><center>

						<center><a href=""><img class="img-responsive btns-verinfo" src="<?= base_url()?>tema/imgs/svg/verinfo.svg"></a></center>

					</div>
					
				</div>

				<div class="col-sm-3">
					<div class=" borde-rojo">
						<img class="  img-responsive img-amortiguador-catalogo" src="<?= base_url()?>tema/imgs/amort2.png">

						<center><p class="HelveticaNeueLTStd-Lt">Amortiguadores <br>Excel-G</p><center>

						<center><a href=""><img class="img-responsive btns-verinfo" src="<?= base_url()?>tema/imgs/svg/verinfo.svg"></a></center>
					</div>
					
					
				</div>

				<div class="col-sm-3   ">
					<div class="  borde-rojo">
						<img class="   img-responsive img-amortiguador-catalogo" src="<?= base_url()?>tema/imgs/amort3.png">

						<center><p class="HelveticaNeueLTStd-Lt">Amortiguadores <br>Gas-Just</p><center>

						<center><a href=""><img class="img-responsive btns-verinfo" src="<?= base_url()?>tema/imgs/svg/verinfo.svg"></a></center>
					</div>
					
					
				</div>

				<div class="col-sm-3   ">
					<div class=" borde-rojo ">
						<img class="  img-responsive img-amortiguador-catalogo" src="<?= base_url()?>tema/imgs/amort4.png">

						<center><p class="HelveticaNeueLTStd-Lt">Amortiguadores <br>Klassic</p><center>

						<center><a href=""><img class="img-responsive btns-verinfo" src="<?= base_url()?>tema/imgs/svg/verinfo.svg"></a></center>
					</div>
					
					
				</div>
			</div>
		</div>
		        	
     
    </div>


    <!--Contacto-->

     <hr style="border:3px solid #fff ;margin:0px">

    <div class="container-fluid contacto" id="contacto">

    	<div class="container">
    		<div class="row">
    			<h1 class="amortiguadores2-title">contacto</h1>
    		</div>
    	</div>


    	<div class="row">
    		<div class="col-sm-6  ">
    			<div class="row">
    				<img class ="icono_contacto_footer" src="<?= base_url()?>tema/imgs/svg/telefono.svg">
    				<p class="p-contacto"> TELÉFONO<br> <a href="tel:+50622220000">(506) 2222-0000 (LÍNEA DE ASISTENCIA</a> </p>
    			</div>

    			<div class="row">
    				<img class ="icono_contacto_footer" src="<?= base_url()?>tema/imgs/sobre.png">
    				<p class="p-contacto"> CORREO ELECTRÓNICO <br> <a href="mailto:info@kybcr.com">info@kybcr.com</a></p>
    			</div>

    			<div class="row">
    				<img class ="icono_contacto_footer" src="<?= base_url()?>tema/imgs/svg/puntosventa.svg">
    				<p class="p-contacto"> <a href="#">VER PUNTOS DE VENTA</a> </p>
    			</div> 
    		</div>

    		<div class="col-sm-6  ">
    			<div class="row">

    				<input class="input-contacto" type="text" name="nombre" placeholder="NOMBRE COMPLETO">
    				
    			</div>

    			<div class="row">
    				<input  class="input-contacto" type="text" name="nombre" placeholder="CORREO ELECTRÓNICO">
    				
    			</div>

    			<div class="row">
    				<textarea class="input-contacto" rows="4" cols="50" placeholder="MENSAJE"></textarea>
    				
    			</div>

    			<div class="row">
    				
    				<a style="width: 90%;margin-left: 35px;" href=""><img class="img-responsive enviar-contacto-btn " src="<?= base_url()?>tema/imgs/svg/enviar.svg"></a>
    			</div>
    		</div>

    		<!-- <div class="col-sm-4  ">
    			
    			<div class="col-sm-12 redes-footer">


    				<div class="col-sm-4">
    					<a href=""><img src="<?= base_url()?>tema/imgs/facebookblanco.png"></a>
    				</div>

    				<div class="col-sm-4">
    					<a href=""><img src="<?= base_url()?>tema/imgs/yt.png"></a>
    				</div>

    				<div class="col-sm-4">
    					<a href=""><img src="<?= base_url()?>tema/imgs/ws.png"></a>
    				</div>

    				
    				
    			</div>

    			<div class="row">
    				<div class="ver-catalogo=footer">
    					<a href=""><img class="catalogo-img-footer" src="<?= base_url()?>tema/imgs/svg/catalogo.svg"></a>
    					<p class="p-contacto inline-custom-footer">Ver Catálogo</p>


    				</div>
    			</div>

    			<div class="row">

    				<div class="ver-infotec=footer">
    					<a href=""><img class="infotecnica-img-footer" src="<?= base_url()?>tema/imgs/svg/infotecnica.svg"></a>
    					<p class="p-contacto inline-custom-footer">Ver Información Técnica</p>
    				</div>
    				
    			</div>
    		</div> -->
    	</div>
    	
    </div>

    <div class="footer-redessociales">
        <div class="col-sm-12 redes-footer">

        	
        		 <center><a href=""><img src="<?= base_url()?>tema/imgs/facebookblanco.png"></a>
                 <a href=""><img src="<?= base_url()?>tema/imgs/yt.png"></a>
                 <a href=""><img src="<?= base_url()?>tema/imgs/ws.png"></a></center>
        	
                

        </div>

    </div>

    <div class="footer-pixel">
        <div class="txt-center">
                  <p style="display:inline-block;color:#fff;text-transform:capitalize">Diseño Por:</p>
                  <a href="https://www.pixelcr.com/" target="_blank">

                  <img src="http://pixelcr.com/credits/pixel-logo-blanco.png" id="logopixel" class="img-responsive" alt="">
                  </a>

        </div>

    </div>
 		









<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="<?= base_url()?>tema/js/bootstrap.js"></script>
  
<script type="text/javascript" src="<?= base_url()?>tema/js/slick.js"></script>


<script type="text/javascript">
    $(document).ready(function(){
      $('.slider-header').slick({
          
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  adaptiveHeight: true,
		  

      });
    });
  </script>
</body>

</html>